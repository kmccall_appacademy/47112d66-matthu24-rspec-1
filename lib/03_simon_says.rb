def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, repeat = 2)
  result = ""
  (1..repeat).each do |num|
      result += string
      result += " "
  end
  result[0...result.length-1]
end

def start_of_word(string,num)
  last_index = num - 1
  string[0..last_index]
end

def first_word(string)
  string.split(" ")[0]
end


def titleize(string)
  string_array = string.split
  string_array[0].capitalize!
  string_array.each do |word|
    word.capitalize! unless word == "and" || word == "over" || word =="the"
  end

  string_array.join(" ")
end
