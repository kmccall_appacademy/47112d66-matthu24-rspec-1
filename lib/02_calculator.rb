def add(num1,num2)
  num1+num2
end

def subtract(num1,num2)
  num1-num2
end

def sum(array)
  sum = 0
  array.each do |num|
    sum+=num
  end
  sum
end

def multiply(num1,num2)
  num1*num2
end

def power(base,power)
  base**power
end

def factorial(n)
  return 1 if n == 0
  n*factorial(n-1)
end
