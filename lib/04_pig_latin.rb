def translate_word(word)
  vowels = ["a","e","i","o","u"]
  base_word = word
  first_consonants = ""

  while true
    #test if letter is a consonant, OR if it is a "u" following a "q"
    if vowels.include?(base_word[0]) == false || (base_word[0] == "u" && first_consonants.include?("q"))
      #add consonant to first_consonants
      first_consonants += base_word[0]
      #subtract consonant from translated_word
      base_word.slice!(0)
    else
      break
    end
  end

  base_word + first_consonants + "ay"
end

def translate(string)
  translated_array = []
  string_array = string.split(" ")
  string_array.each do |word|
    translated_array << translate_word(word)
  end

  translated_array.join(" ")
end
